import sqlite_backend
import config

class Model(object):

    def __init__(self, first_name = None, last_name = None, listData = None, conn = None):
        self.first_name = first_name
        self.last_name = last_name
        self.listData = listData
        self.conn = conn

    def CreateDBModel(self):
        self.conn = sqlite_backend.connect_to_db(config.DB_name)
        return self.conn

    def createTableModel(self):
        self._item_type = """
                            CREATE TABLE region (
                                id INTEGER PRIMARY KEY,
                                region TEXT NOT NULL,
                                city_name TEXT NOT NULL,
                                languages TEXT NOT NULL,
                                time TEXT NOT NULL
                            );
                            """
        self.conn = sqlite_backend.connect_to_db(config.DB_name)
        return sqlite_backend.create_table(self.conn, self._item_type)

    def getAllModel(self):
        self.conn = sqlite_backend.connect_to_db(config.DB_name)
        sql = 'SELECT * FROM region'
        c = self.conn.execute(sql)
        results = c.fetchall()
        return list(map(lambda x: sqlite_backend.tuple_to_dict(x), results))

    def saveAllModel(self, data):
        self.conn = sqlite_backend.connect_to_db(config.DB_name)
        table_name = sqlite_backend.scrub("region")
        sql = "INSERT INTO {} ('region', 'city_name', 'languages', 'time') VALUES (?, ?, ?, ?)"\
            .format(table_name)
        entries = list()
        try:
            for x in data:
                lang = ''.join(str(e) for e in x['languages'])
                entries.append((x['region'], x['city_name'], lang, x['time']))
            self.conn.executemany(sql, entries)
            self.conn.commit()
            return "Se ha registrado con exito"
        except Exception as e:
            return e, "Error"