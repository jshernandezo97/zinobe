from sqlite3 import ProgrammingError
import sqlite3
import config



def scrub(input_string):
    """
    Clean an input string (to prevent SQL injection).
    Parameters
    ----------
    input_string : str

    Returns
    -------
    str
    """
    return ''.join(i for i in input_string if i.isalnum())

def tuple_to_dict(mytuple):
    mydict = dict()
    mydict['id'] = mytuple[0]
    mydict['region'] = mytuple[1]
    mydict['city_name'] = mytuple[2]
    mydict['language'] = mytuple[3]
    mydict['time'] = mytuple[4]
    return mydict    


def connect_to_db(base_datos):
    try:
        conexion = sqlite3.connect(base_datos)

        return conexion
    except sqlite3.Error as error:
        print('Se ha producido un error al crear la conexión:', error)


def connect(func):
    """Decorator to (re)open a sqlite database connection when needed.

    A database connection must be open when we want to perform a database query
    but we are in one of the following situations:
    1) there is no connection
    2) the connection is closed

    Parameters
    ----------
    func : function
        function which performs the database query

    Returns
    -------
    inner func : function
    """
    def inner_func(conn, *args, **kwargs):
        try:
            # I don't know if this is the simplest and fastest query to try
            conn.execute(
                'SELECT name FROM sqlite_temp_master WHERE type="table";')
        except (AttributeError, ProgrammingError):
            conn = connect_to_db(config.DB_name)
        return func(conn, *args, **kwargs)
    return inner_func        
    
def create_table(conn, definicion):
    try:
        cursor = conn.cursor()
        cursor.execute(definicion)
        conn.commit()
        return "La tabla se creo correctamente"
    except Exception as e:
        return "Error al crear la tabla", e



@connect
def insert_many(conexion, items, table_name):
    table_name = scrub(table_name)
    sql = "INSERT INTO {} ('region', 'city_name', 'languages', 'time') VALUES (?, ?, ?, ?)"\
        .format(table_name)
    entries = list()
    for x in items:
        lang = ''.join(str(e) for e in x['languages'])
        entries.append((x['region'], x['city_name'], lang, x['time']))
    try:
        conexion.executemany(sql, entries)
        conexion.commit()
        return "Se ha registrado con exito"
    except Exception as e:
        return e    


@connect
def select_all(conn, table_name):
    table_name = scrub(table_name)
    sql = 'SELECT * FROM {}'.format(table_name)
    c = conn.execute(sql)
    results = c.fetchall()
    return list(map(lambda x: tuple_to_dict(x), results))    


conexion = connect_to_db(config.DB_name)

if conexion:
    conexion.close()
