from model import Model
from view import View
import requests
import json
import config
import time
import hashlib;
import pandas as pd


def createTableController():
    model = Model()
    tbl_zinobe = model.createTableModel()
    return View.showCreateTblView(tbl_zinobe)

def getAPIController():
    url =  config.urlGetRegion
    response = requests.request("GET", url)
    json_response = json.loads(response.text)
    format_data = createDataController(json_response)
    saveDataController(format_data)
    show_all_data = showAllController()
    DataFramePD(show_all_data)
    create_file = saveDataJsonController(show_all_data)
    return View.showAllMethodGetView(create_file)     

def saveDataJsonController(data):
    f= open("data.json","w+")
    f.write(str(data))
    return "Se ha generado el archivo data.json correctamente"

def createSignatureController(data):
    return hashlib.sha1(str(repr(data) + "," + config.shared_private_key).encode('utf-8')).hexdigest()

def createDataController(data):
    data_dict = {
        "seconds": 0,
        "start_time": 0,
        "end_time": 0,
        "listData": [],
        "languages": []
    }
    for val in data:
        data_dict["start_time"] = time.time()
        try:
            for lang in val["languages"].values():
                encode = createSignatureController(lang)
                data_dict["languages"].append(encode)
        except Exception as ex:
            data_dict["languages"].append("No aplica")
            pass
        data_dict["end_time"] = time.time()
        seconds = data_dict["end_time"] - data_dict["start_time"]
        data_dict["listData"].append({ "region": val["region"], "city_name": val["name"]["common"], "languages": data_dict["languages"], "time": seconds})
        data_dict["languages"] = []
    return data_dict["listData"]

def saveDataController(data):
    model = Model()
    tbl_zinobe = model.saveAllModel(data)
    return tbl_zinobe

def showAllController():
    model = Model()
    data_zinobe = model.getAllModel()
    return data_zinobe

def CreatedDB():
    
    try:
        model = Model()
        db_zinobe = model.CreateDBModel()
        createTableController()
        getAPIController()
        return View.showCreateDBView(db_zinobe)
    except Exception as e:
        return View.showCreateDBView(e)    
    
def DataFramePD(data):
    df = pd.DataFrame(data, columns=["region", "city_name", "language", "time"])
    df.describe(include = 'all')
    return View.DataFrameView(df)


if __name__ == "__main__":
    CreatedDB()