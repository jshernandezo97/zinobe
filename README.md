#ZINOBE

## ENUNCIADO
Desarrolle una aplicacion en python que genere la tabla anterior teniendo las siguientes consideraciones:

1. De `https://restcountries.com/`  obtenga el nombre del idioma que habla el pais y encriptelo con SHA1
2. En la columna Time ponga el tiempo que tardo en armar la fila (debe ser automatico)
3. La tabla debe ser creada en un DataFrame con la libreria PANDAS
4. Con funciones de la libreria pandas muestre el tiempo total, el tiempo promedio, el tiempo minimo y el maximo que tardo en procesar toda las filas de la tabla.
5. Guarde el resultado en sqlite.
6. Genere un Json de la tabla creada y guardelo como data.json
7. La prueba debe ser entregada en un repositorio git.

#Es un plus si:

No usa famework
Entrega Test Unitarios
Presenta un diseño de su solucion.

## DESCRIPCION DE LA SOLUCION
Se realizó una petición GET a la ruta `https://restcountries.com/` en la cual se obtuvo la lista de los datos de la región el país y sus lenguas y posteriormente se realizo la encriptación del lenguaje en SHA1 y se tomo el tiempo que tardo el programa en encriptarlo y se guardo en la base de datos `zinobe.bd`, luego se realizó la consulta a BD para obtener los registros y mostrarlos a través de un DataFrame en pandas.

## INSTRUCCIONES PARA CORRER EN AMBIENTE LOCAL
### RUN APP
1. Tener docker instalado posteriormente descargas la imagen de `https://hub.docker.com/repository/docker/johanhdez/zinobe`  con el siguiente comando
`docker pull johanhdez/zinobe:v1`
2. Para ejecutarlo hacer un docker run  `docker run -p 8000:8000  johanhdez/zinobe:v1`

3. Posterior a eso deberá funcionar adecuadamente el aplicativo
